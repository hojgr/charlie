(ns charlie.travian.buildings
  (:require [net.cgrand.enlive-html :as html]))

(defn- is-building?
  [dom-item]
  (not= "dorf2.php" (:href (:attrs dom-item))))

(defn- extract-id
  [url]
  (Integer/parseInt
    (second
      (re-find #"id=([0-9]+)" url))))

(defn- can-upgrade
  [title]
  (not= nil (re-find #"upgrad" title)))

(defn- extract-title
  [title]
  (let [[_ name level] (re-find #"([a-zA-Z0-9 ]+)</b> Level ([0-9]+)" title)]
    {:name name
     :level (Integer/parseInt level)
     :can-upgrade (can-upgrade title)}))

(defn- area->building
  [c]
  (let [id (-> c :attrs :href extract-id)
        et (-> c :attrs :title extract-title)]
    {:name (:name et)
     :level (:level et)
     :can-upgrade (:can-upgrade et)
     :id id}))

(defn get-buildings
  [dorf-dom]
  (let [areas (html/select dorf-dom [:area])
        filtered-areas (filter is-building? areas)]
    (map area->building filtered-areas)))

