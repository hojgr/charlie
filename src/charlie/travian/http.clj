(ns charlie.travian.http
  (:require [clj-http.client :as client]))


(def url "http://tx1024.atergatis.com")
(def login-url (str url "/login.php"))
(def dorf1-url (str url "/dorf1.php"))

(def cookie-store (clj-http.cookies/cookie-store))

(defn get-login
  []
  (client/get login-url {:cookie-store cookie-store}))

(defn get-dorf1
  []
  (client/get dorf1-url {:cookie-store cookie-store}))

(defn login
  [username password token]
  (client/post login-url {:form-params   {:ft    "a4"
                                          :user  username
                                          :pw    password
                                          :s1    "Login"
                                          :w     ""
                                          :login token}
                           :cookie-store cookie-store}))

(defn get-building
  [id]
  (client/get (str url "/build.php?id=" id) {:cookie-store cookie-store}))

(defn get
  [path]
  (client/get
    (str url (clojure.string/replace path "/" ""))
    {:cookie-store cookie-store}))