(ns charlie.travian.auth
  (:require [net.cgrand.enlive-html :as html]
            [charlie.travian.common :as common]
            [charlie.travian.http :as http]))

(defn- get-login-token
  []
  (let [dom (common/response->dom (http/get-login))
        selector [[:input (html/attr= :name "login")]]
        tags (html/select dom selector)]
    (-> tags first :attrs :value)))

(defn login
  [username password]
  (http/login username password (get-login-token)))