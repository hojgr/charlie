(ns charlie.travian.resources
  (:require [charlie.travian.html.common :as common]
            [net.cgrand.enlive-html :as html]))

(defn get-warehouse-capacity
  [html]
  (Long/parseLong
    (first
     (html/select html [:#stockBarWarehouse :> html/text-node]))))

(defn get-granary-capacity
  [html]
  (Long/parseLong
    (first
     (html/select html [:#stockBarGranary :> html/text-node]))))

(defn get-free-crop
  [html]
  (Long/parseLong
    (first
     (html/select html [:#stockBarFreeCrop :> html/text-node]))))

(defn get-stored-resources
  [html]
  (let [[lumber clay iron crop _] (html/select html [:.stockBarButton :.value :> html/text-node])]
    {:lumber lumber
     :clay clay
     :iron iron
     :crop crop}))

(defn get-production
  [dorf1-html]
  (let [[lumber clay iron crop] (html/select dorf1-html [:#production :tbody :tr :.num :> html/text-node])]
    {:lumber lumber
     :clay clay
     :iron iron
     :crop crop}))