(ns charlie.travian.common
  (:require [net.cgrand.enlive-html :as html]))

(def response->dom #(-> % :body html/html-snippet))